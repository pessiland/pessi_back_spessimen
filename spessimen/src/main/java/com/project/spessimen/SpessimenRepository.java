package com.project.spessimen;

import com.project.spessimen.model.Spessimen;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SpessimenRepository extends JpaRepository<Spessimen, Integer> {

    List<Spessimen> findAll();

    Spessimen findSpessimenByIdSpessimen(int idSpessimen);
}


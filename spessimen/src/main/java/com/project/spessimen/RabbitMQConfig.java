package com.project.spessimen;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Bean
    public DirectExchange spessimenExchange() {
        return new DirectExchange("spessimen-exchange");
    }

    @Bean
    public Queue incubationQueue() {
        return new Queue("incubation-queue");
    }


    @Bean
    public Binding incubationBinding(Queue incubationQueue, DirectExchange spessimenExchange) {
        return BindingBuilder.bind(incubationQueue).to(spessimenExchange).with("incubation-queue");
    }
}




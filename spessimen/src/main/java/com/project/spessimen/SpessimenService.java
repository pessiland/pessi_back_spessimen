package com.project.spessimen;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.spessimen.model.Spessimen;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class SpessimenService {
    private final SpessimenRepository spessimenRepository;
    private final AmqpTemplate rabbitTemplate;

    @Autowired
    public SpessimenService(SpessimenRepository spessimenRepository, AmqpTemplate rabbitTemplate) {
        this.spessimenRepository = spessimenRepository;
        this.rabbitTemplate = rabbitTemplate;
    }

    public List<Spessimen> getAllSpessimen() {
        return spessimenRepository.findAll();
    }

    public Spessimen getSpessimenById(int id) {
        return spessimenRepository.findSpessimenByIdSpessimen(id);
    }

    @RabbitListener(queues = "incubation-queue")
    public void handleSpessimenGenerationRequest(String playerId) {
        try {
            Spessimen generatedSpessimen = generateRandomSpessimen();

            ObjectMapper objectMapper = new ObjectMapper();
            String jsonSpessimen = objectMapper.writeValueAsString(generatedSpessimen);

            System.out.println("Okey ça part");
            rabbitTemplate.convertAndSend("team-exchange", "team-queue", new Message(jsonSpessimen.getBytes(), new MessageProperties()), message -> {
                message.getMessageProperties().setHeader("playerId", playerId);
                return message;
            });
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public Spessimen generateRandomSpessimen() {
        List<Spessimen> allSpessimens = spessimenRepository.findAll();

        if (allSpessimens.isEmpty()) {
            return null;
        }

        int randomIndex = new Random().nextInt(allSpessimens.size());
        return allSpessimens.get(randomIndex);
    }
}

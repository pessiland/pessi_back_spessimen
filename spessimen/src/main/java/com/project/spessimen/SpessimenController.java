package com.project.spessimen;

import com.project.spessimen.model.Spessimen;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/spessidex")
public class SpessimenController {

    private final SpessimenService spessimenService;

    public SpessimenController(SpessimenService spessimenService) {
        this.spessimenService = spessimenService;
    }

    @GetMapping("/all")
    public List<Spessimen> getAllSpecimen() {
        return spessimenService.getAllSpessimen();
    }

    @GetMapping("/{id}")
    public Spessimen getSpessimenById(@PathVariable int id) {
        return spessimenService.getSpessimenById(id);
    }
}

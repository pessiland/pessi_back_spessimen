package com.project.spessimen.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;

@Entity
@Table(name = "Spessimen")
public class Spessimen {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_spessimen")
    private int idSpessimen;

    @Column(name = "nom", length = 45)
    private String name;

    @ManyToOne
    @JoinColumn(name = "type", referencedColumnName = "id_type")
    private Type type;

    @Column(name = "pv")
    private int pv;

    @ManyToOne
    @JoinColumn(name = "rarete", referencedColumnName = "id_rarete")
    private Rarete rarete;

    @Column(name = "description", length = 1000)
    private String description;

    @JsonProperty("idSpessimen")
    public int getIdSpessimen() {
        return idSpessimen;
    }

    @JsonProperty("Nom")
    public String getName() {
        return name;
    }

    @JsonProperty("Type")
    public String getType() {
        return type.getName();
    }

    @JsonProperty("PV")
    public int getPv() {
        return pv;
    }

    @JsonProperty("Rarete")
    public String getRarete() {
        return rarete.getName();
    }

    @JsonProperty("Description")
    public String getDescription() {
        return description;
    }
}


package com.project.spessimen.model;

import jakarta.persistence.*;

@Entity
@Table(name = "Rarete")
public class Rarete {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_rarete")
    private int idRarete;

    @Column(name = "nom_rarete", length = 45)
    private String name;

    public String getName() {
        return this.name;
    }
}

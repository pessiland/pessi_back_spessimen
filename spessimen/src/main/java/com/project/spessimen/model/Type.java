package com.project.spessimen.model;

import jakarta.persistence.*;

@Entity
@Table(name = "Type")
public class Type {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_type")
    private int idType;

    @Column(name = "nom_type", length = 45)
    private String name;

    public String getName() {
        return this.name;
    }
}
